/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require("fs");
const path = require("path");
const { pipeline } = require("stream");

const deleteFiles = (data) => {
    const filenamesArr = data.split(".txt");
    filenamesArr.pop();
    const fileNamesWithExtension = filenamesArr.map((filename) => {
        return filename + ".txt";
    })
    // console.log(fileNamesWithExtension);
    const filesPath = fileNamesWithExtension.map((filename) => {
        return path.join(__dirname, filename);
    });
    // console.log(filesPath);
    filesPath.map((file) => {
        fs.unlink(file, (err) => {
            if(err){
                console.log(err);
            }
            else{
                console.log(`${path.basename(file)} deleted!`);
            }
        })
    })    
};

const readFilenames = (fileName = "filenames.txt") => {    
    const filePath = path.join(__dirname, fileName);

    fs.readFile(filePath, "utf-8", (err, data) => {
        if(err){
            console.log(err);
        }
        else{
            // console.log(data);
            deleteFiles(data);
        }
    })

}

const storeFileName = (nameOfFile) => {
    // nameOfFile += "\n";
    const fileToStoreIn = "filenames.txt";
    const filePath = path.join(__dirname, fileToStoreIn);
    fs.appendFile(filePath, nameOfFile, (err) => { // new file name needs to be added not overwritten!
        if (err) {
            console.log(err);
        }
        else {
            console.log(`Filename ${nameOfFile} stored in ${fileToStoreIn}`);
        }
    })
}

const dataSort = (data) => {
    let sortedData = data.split("");
    let editedData = sortedData.map((char) => { //removing unwanted characters such as ",/./\n"
        if (char === "," || char === ".") {
            return char = "";
        } else if (char === "\n") {
            return char = " ";
        }
        else {
            return char;
        }
    })
    editedData = editedData.join("").split(" ").sort().join(" ").trim();
    // console.log(editedData);

    const fileName = "lipsumSorted.txt";
    const filePath = path.join(__dirname, fileName);
    fs.writeFile(filePath, editedData, (err) => {
        if (err) {
            console.log(err);
        }
        else {
            console.log(`data written in ${fileName}`)
            storeFileName(fileName);
            readFilenames();
        }
    })
}

const readSplitLipsum = (fileName) => {
    const filePath = path.join(__dirname, fileName);
    fs.readFile(filePath, "utf-8", (err, data) => {
        if (err) {
            console.log(err);
        }
        else {
            console.log(`data from ${fileName} read!`);
            dataSort(data);
        }
    })
}

const dataToLowerCase = (data) => {
    const lowerCaseData = data.toLowerCase();
    const sentencesSplit = lowerCaseData.split(". ");
    // console.log(sentencesSplit);
    const sentencesSplitJoin = sentencesSplit.join("\n");
    // console.log(sentencesSplitJoin);
    const fileName = "lipsumLowerSplit.txt";
    const filePath = path.join(__dirname, fileName);
    fs.writeFile(filePath, sentencesSplitJoin, (err) => {
        if (err) {
            console.log(err);
        }
        else {
            console.log(`data written in ${fileName}`)
            storeFileName(fileName);
            readSplitLipsum(fileName);
        }
    })
}

const readUlipsumFile = (fileName) => {
    const filePath = path.join(__dirname, fileName);
    fs.readFile(filePath, "utf-8", (err, data) => {
        if (err) {
            console.log(err);
        }
        else {
            console.log(`data from ${fileName} read!`);
            dataToLowerCase(data);

        }
    })
}

const dataToUpperCase = (data) => {
    const upperCaseData = data.toUpperCase();
    // console.log(upperCaseData);
    const fileName = "lipsumUpper.txt";
    const filePath = path.join(__dirname, fileName);
    fs.writeFile(filePath, upperCaseData, (err) => {
        if (err) {
            console.log(err);
        }
        else {
            console.log(`data written in ${fileName}`)
            storeFileName(fileName);
            readUlipsumFile(fileName);
        }
    })
}

const readlipsum = (fileName) => {
    const filePath = path.join(__dirname, fileName);
    fs.readFile(filePath, "utf-8", (err, data) => {
        if (err) {
            console.log(err);
        }
        else {
            console.log(`data from ${fileName} read!`);
            dataToUpperCase(data);
        }
    })
}

const problem2Function = (fileName) => {
    readlipsum(fileName);
}

module.exports = problem2Function;