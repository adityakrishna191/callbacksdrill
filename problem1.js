/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/

const fs = require("fs");
const path = require("path");

const deleteFiles = (dirName) => {
    fs.readdir(dirName, (err, files) => {
        if (err) {
            console.log(err);
        }
        else {
            // console.log(files);
            return files.map((file) => {
                const filePath = path.join(dirName, file);
                // console.log(filePath);
                fs.unlink(filePath, (err) => {
                    if (err) {
                        console.log(err);
                    }
                    else {
                        console.log(`${file} deleted!`);
                    }
                });
            })
        }
    });
}

const createJsonFiles = (dirPath, dirName) => { // jsonFileList for modularity
    const file1Name = "file1.json";
    const file1Path = path.join(dirPath, file1Name);
    const file2Name = "file2.json";
    const file2Path = path.join(dirPath, file2Name);
    const file3Name = "file3.json";
    const file3Path = path.join(dirPath, file3Name);
    const file4Name = "file4.json";
    const file4Path = path.join(dirPath, file4Name);
    fs.writeFile(file1Path, JSON.stringify({ "key1": "value1" }), err => {
        if (err) {
            console.log(err);
        }
        else {
            console.log(`file1.json created!`);
        }
    })
    fs.writeFile(file2Path, JSON.stringify({ "key2": "value2" }), err => {
        if (err) {
            console.log(err);
        }
        else {
            console.log(`file2.json created!`);
        }
    })
    fs.writeFile(file3Path, JSON.stringify({ "key3": "value3" }), err => {
        if (err) {
            console.log(err);
        }
        else {
            console.log(`file3.json created!`);
        }
    })
    fs.writeFile(file4Path, JSON.stringify({ "key4": "value4" }), err => {
        if (err) {
            console.log(err);
        }
        else {
            console.log(`file4.json created!`);
        }
    })

    deleteFiles(dirName);
}

const createDir = (dirName) => {

    const dirPath = path.join(__dirname, dirName);
    // console.log(dirPath);

    fs.mkdir(dirPath, { recursive: true }, (err) => {
        if (err) {
            console.log(err)
        }
        else {
            createJsonFiles(dirPath, dirName);
            console.log(`"${dirName}" created!`)
        }
    })
}

const problem1Function = (dirName) => {
    createDir(dirName);
}

module.exports = problem1Function;